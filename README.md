# The Land Chain
A blockchain side implementation where we track the transfer of land titles after a real-life purchase has been made. Made by Navid Bhuiyan, Thomas French, Kane Jackson and Bashir Bandi.

## How it works?
The code base works based on assuming a sequence of user events which would implemented on the frontend. Based on our presentation,
our final product is able to interact with different land titles, create land titles, track and modify land values, produce estimates on capital gains taxes, and help digitise the transfer of land title ownwership.

**Please compile contract-attribute/Attribute.sol, contract-interface/Interface.sol, 'contract-tax-oracle/TaxOracle.sol & contract-value-oracle/ValueOracle.sol if you want to test the code locally. Run each oracle js script with the node command in seperate terminals.**

### Land Title Interace
Interface.sol wraps all of our contracts together for a front-end interface to interact with our blockchain application. Here are the following functions which can be used to interact with the land titles and manage their addresses.

- getLandTitleDatabaselength() -> Gets number of landtitles
- findLandTitle(string memory landAddress) OR findLandTitle(uint index) -> Gets exact contract address
- registerLandTitle(string memory landAddress) -> Create land title
- removeLandTitle(string memory landAddress) -> Remove our record of the land title.

This assumes that only one contract can exist for each land title. This interface is used to simplify how a front-end application manages the off-chain database collating the overall number of contracts. 

### Creation of Land Titles (i.e. virtual contracts)
As of now our blockchain application requires manual input to create new contracts, as this is based on the assumption that an external backend and frontend interface will sequence steps to create seperate contracts. As of now our contracts are based on the assumption on a one to many assumption (i.e. one owner can have multiple properties, but one property can only have one owner). There are two ways to create land titles.

The first way, is via Interface.sol through the registerLandTitle which takes in an address and generates a contract.

To create a land title, simply go into remix, select the LandTitle.sol, compile then launch with Metamask on the Ropsten testnet (i.e. an Etheruem testnet).

After this, the following actions will have to commence in sequence to ensure the rest of the functions provide valuable output.
1. setLandAddress (real residential land addresses formatted like this -> 15 Fingleton St Imaginarysuburb 1001)
2. updateLandValue

This then becomes functional for the rest of the features to execute, as you become the owner of the land title.

### Tracking and Modifying Land Values
Land values are simply tracked through the addition of attributes via the addAttributes function and via the land value oracle.

There are three attribute of type ENUM (0 -> GENERAL, 1 -> DAMAGE, 2 -> RENNOVATION), which either add or detract from the base land value. The addAttributes fuction requires the attribute type (int), added or substracted land value (int), and a description of the change.

The oracle scripted in landValueProvider.js and the contract interfacing with LandTitle.sol in ValueOracle.sol interact with a third party API to add a base value. The oracle can be invoked via the requestLandValue function. However there is another function which may be used in edge cases to overwrite the land value via the updateLandTitle function which accepts an integer input.

### Capital Gains Tax Estimates
Taxes are calculated from another oracle scripted in both TaxOracle.sol and taxOracle.js. These mainly connect the blockchain to an external machine which calculates the capital gains tax on a given property. As of now only the owner of the land title can manually invoke it, however
using an external backend and a frontend, it may be invoked as a land title is transferred.

To find the capital gains taxes, you can invoke the requestLandTax function to pre-calculate taxes. Also there is a override function in user edge cases where actual vs estimated tax may differ and can be invoked to update the paid capital gains taxes on the contract.

### Digitising Land Title Ownership Transfer
The land title ownership transfer assumes a step of sequences between the seller and purchaser and that are in contact with the entire process. There are ENUMs representing the state of the property (via the TRANSFER_STATE variable) which translates into three stages: NOT_FOR_SALE, FOR_SALE, PURCHASE_READY and then cycles back to NOT_FOR_SALE, which is the land titles default state. The actions are assumed in the following steps for a successful purchase:

1. The seller initiates their land for sale via the requestSale function to bring it to a  state.
2. The buyer then has the address of the land title (requested from the seller), to then use the requestOwnership function. If successful then the state of the contract becomes PURCHASE_READY.
3. The seller then verifies the address of the buyer requesting the land title via the approveOwnership function, then the transfer is complete and the state of the land title is back to NOT_FOR_SALE.

However, there are two additional functions for the owner to block request (via the blockOwnership function) for ownership (which brings it to a FOR_SALE state) or cancels the sale (via the cancelSale function) function to bring it back to a NOT_FOR_SALE state.

These states are connected with modifiers which restrict and open other functions which limit how a user interacts with the contract. This is seen through the modifiers.

## Other Caveats
There are two points to discuss why for this MVP there were final changes in our decisions to go and limit the scope of the application.

- One-to-many owner to land title relationship
- Unisgned tnteger values for land evaluation and taxes

For point one, we limited this due to technical feasibility and project management which restricted possibly implementing a version of the code which could handle many owners at once. Also having multiple owners and somehow managing the transactions to allow for integrity and reliability is complex enough for this task.

For point two, its simply negligible at large values to be considered useful for the scope of the application since we do not actually manage the purchase of land titles itself, rather track the ownership. Considering memory and integrity of float values could have on our contracts and to reduce the potential gas price by processing integer instead of float values.
