const Web3 = require("web3");
require("dotenv").config();
const OracleAddress = '0x84db9a932b5faa8c5946bc648d104776e02b52de'


// Loading the contract ABI
// (the results of a previous compilation step)
const fs = require("fs");
// loads ABI for tax oracle <-- data is recieved from here
const abi = JSON.parse(fs.readFileSync("TaxOracleABI.json"));
// loads ABI for land title <-- calculated tax is input here
const abi2 = JSON.parse(fs.readFileSync("LandTitleABI.json"));
var price; 
var tax;
var taxp;


const network = process.env.ETHEREUM_NETWORK;
var web3 = new Web3(Web3.givenProvider || 'wss://ropsten.infura.io/ws/v3/8d6931acd3a04dabb8848c6c345ee755');


// Creating a Oracle Contract instance
const contract = new web3.eth.Contract(
  abi, OracleAddress
);


// Creating a signing account from a private key

const signer = web3.eth.accounts.privateKeyToAccount(
  process.env.SIGNER_PRIVATE_KEY_TAX
);
web3.eth.accounts.wallet.add(signer);


// returns calculated tax to landtitle.sol
async function returnValueToOracle(address, value) {
  const tx = contract.methods.returnTaxValue(address, value);
  const receipt = await tx
      .send({
          from: signer.address,
          gas: await tx.estimateGas() + 50000,
      })
      .once("transactionHash", (txhash) => {
          console.log(`Mining transaction ...`);
          console.log(`https://ropsten.etherscan.io/tx/${txhash}`);
          console.log(`Transaction successful`);
      });
}

async function main() {
  // Configuring the connection to an Ethereum node

let options = {
    fromBlock: 0,
    address: [OracleAddress],    //Only get events from specific addresses
};

let subscription = web3.eth.subscribe("logs", {address: OracleAddress}, (err, result) => {
  let decoded = web3.eth.abi.decodeLog([{
      type: 'address',
      name: 'requestOrigin'
  },{
      type: 'uint256',
      name: 'landValue'
  },{
      type: "string",
      name: "landAddress"
  }

], result.data, result.topics);
  console.log('\n\nvalue recieved from oracle\n\n');
  console.log(decoded)
  
  
  //tax % - future: can be input via tax office UI
  taxp = 10;

  // tax calculation
  price = decoded[1]; //gets price from recieved data
  tax = price * (taxp/100);  //converts tax p to % and calculates tax

  console.log("tax calculated\n\n")

  //returns tax value to landtitle
  returnValueToOracle(decoded['2'], tax);

});

}

main();
