#!/bin/bash

# Load environment variables
export SIGNER_PRIVATE_KEY_TAX=99cdf8087785916516910b3baf5094d3bb4f32056d3b9955e66a270ee701082f
export SIGNER_PRIVATE_KEY_VALUE=24c81c5dfdc2723834db8fbc3d4dee55b7f89b0056ff4efa760206fdb9621eb7

# Load node instances
npm install

# Load shell instances
echo Running Oracles
/bin/bash -ec 'node taxOracle.js'
/bin/bash -ec 'node landValueProvider.js'
