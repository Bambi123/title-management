// SPDX-License-Identifier: UNLICENSED

pragma solidity ^0.8.0;
import "../contract-land-title/LandTitle.sol";

/// @title Interface
/// @notice The interface contract is the main application entry point of the land title management process
contract Interface {

    /* The On-Chain Database component that stores the list of land titles within the smart contract */
    LandTitle[] private LandTitleDatabase;
    /* The administrator of the Platform */
    address private admin; 

    
    /// @notice Upon construction of the Interface Contract, the creator is initialised as the administrator
    constructor() {
        admin = tx.origin;
    }

    /* ------------------------
        ADMINISTRATION
     ------------------------ */

    function getAdministrator() public view returns (address){
        return admin;
    }

    /* ------------------------
        LAND TITLE MANAGEMENT
     ------------------------ */

    /// @notice Function to retrieve the size of the Land Title Database
    /// @return an unsigned integer denoting the size of the database
    function getLandTitleDatabaselength() public view returns (uint){
        return LandTitleDatabase.length;
    }

    /// @notice Find the blockchain address of a land title from it's land address
    /// @return If the postal address exists in the database, the blockchain address, otherwise, null
    function findLandTitle(string memory landAddress) public view returns (address) {
        for (uint index = 0; index < LandTitleDatabase.length; index++){
            string memory addr = LandTitleDatabase[index].getLandAddress();
            if (keccak256(abi.encodePacked(addr)) == keccak256(abi.encodePacked(landAddress))){
                return address(LandTitleDatabase[index]);
            }
        }
        return address(0);
    }

    /// @notice Find the blockchian address of a land title from it's index in the database
    /// @return The blockchain address of the index.  If the index is beyond the range of the database, null address is returned
    function findLandTitle(uint index) public view returns (address){
        if (index < 0 || index >= getLandTitleDatabaselength()){return address(0);}
        return address(LandTitleDatabase[index]);
    }

    /// @notice Register a new land title in the system
    /// @return The new contract address of the newly registered land title
    function registerLandTitle(string memory landAddress) public checkLandTitleNoExist(landAddress) checkAdministration returns (address) {
        LandTitle newLandTitle = new LandTitle();
        newLandTitle.setLandAddress(landAddress);
        LandTitleDatabase.push(newLandTitle);
        return address(newLandTitle);
    }

    /// @notice Delete a land title from the existing on-chain database
    /// @return A boolean indicating whether the removal was successful
    function removeLandTitle(string memory landAddress) public checkAdministration returns (bool) {

        /* Initialise Index to a position after the length of the array (position unreachable)*/
        uint indexToDelete = LandTitleDatabase.length;

        /* Find the Index to be removed from the Database */
        for (uint index = 0; index < LandTitleDatabase.length; index++){
            string memory addr = LandTitleDatabase[index].getLandAddress();
            if (keccak256(abi.encodePacked(addr)) == keccak256(abi.encodePacked(landAddress))){
                indexToDelete = index;
            }
        }

        /* Check if the index is valid for deletion */
        if (indexToDelete == LandTitleDatabase.length){return false;}

        /* Otherwise, overwrite addresses and shift all elements */
        for (uint index = indexToDelete; index < LandTitleDatabase.length - 1; index++){
            LandTitleDatabase[index] = LandTitleDatabase[index + 1];
        }

        /* Now that on-chain database has been overwritten, remove last element */
        LandTitleDatabase.pop();
        return true;
    }

    /* ------------------------
        MODIFIERS
     ------------------------ */

    /// @notice Can only execute if the provided land address does not exist in the on-chain database
    modifier checkLandTitleNoExist(string memory landAddress){
        require(address(0) == findLandTitle(landAddress), "This land address already exists!");
        _; 
    }

    /// @notice Can only execute when the caller of the method is the owner of the land title
    modifier checkAdministration() {
        require(tx.origin == admin, "You must be an administrator to execute this method!");
        _;
    }

}