// SPDX-License-Identifier: UNLICENSED

pragma solidity ^0.8.0;

/// @notice Contract to Manage the Attributes on a Land Title
contract Attribute {
    /* The type of attribute */
    enum AttrType{ GENERAL, DAMAGE, RENNOVATION }
    AttrType private attributeType;
    /* The description of the attribute*/
    string private description;
    /* The added value due to the attribute */
    int private addedValue;

    /// @notice Constructor Initialises a new Attribute given only the type of attribute
    constructor () {
        setAttributeType(AttrType.GENERAL);
        setDescription("");
        setAddedValue(0);
    }
    
    /// @notice Gets the type of the attribute
    function getAttributeType() public view returns (AttrType){return attributeType;}
    
    /// @notice Gets the added value of the attribute
    function getAddedValue() public view returns (int){return addedValue;}
    
    /// @notice Gets the description of this attribute
    function getDescription() public view returns (string memory){return description;}
    
    /// @notice Set the type of this attribute
    function setAttributeType(AttrType attrType) public returns (bool){attributeType = attrType; return true;}

    /// @notice Set the added value of this attribute
    function setAddedValue(int amount) public returns (bool) {addedValue = amount; return true;}

    /// @notice Set the description of this attribute
    function setDescription(string memory desc) public returns (bool) {description = desc; return true;}

}