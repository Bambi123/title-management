// SPDX-License-Identifier: UNLICENSED

pragma solidity ^0.8.0;
import "../contract-land-title/LandTitle.sol";

/// @title Oracle handling land value requests
contract ValueOracle {

    mapping(string => address) public pendingRequests;

    /* The event to be broadcast on the blockchain (listened to by off-chain components) */
    event ValueRequest(address requestOrigin, string landAddress);

    /// @notice Call to request a value for given land
    /// @param landAddress The physical address of the land being queried
    function requestLandValue(string memory landAddress) public {
        pendingRequests[landAddress] = msg.sender;
        emit ValueRequest(msg.sender, landAddress);
    }

    /// @notice Return determined value to the 'LandTitle' requesting it
    /// @param landAddress The physical address of the land being queried
    /// @param value The determined value (processed off-chain in landValueProvider.js)
    function returnLandValue(string memory landAddress, int value) public {
        LandTitle returningLandTitle = LandTitle(pendingRequests[landAddress]);
        returningLandTitle.updateBaseLandValue(value);
    }

    /// @notice Instantiates a new ValueOracle smart contract
    constructor () {}

}