// SPDX-License-Identifier: UNLICENSED

pragma solidity ^0.8.0;
import "../contract-attribute/Attribute.sol";
import "../contract-tax-oracle/TaxOracle.sol";
import "../contract-value-oracle/ValueOracle.sol";

/// @title Contract defining a land title
contract LandTitle {

    address public admin;
    string public landAddress;
    address public owner;
    uint private baseValue;
    int private addedValue;
    uint public taxValue;
    // Change and get kane to upload contracts to the blockchain
    ValueOracle public valueOracle = ValueOracle(0x8A7c6De03C283535b3F7A480D68e256c47514c14);
    TaxOracle public taxOracle = TaxOracle(0x84DB9a932B5FaA8C5946bC648d104776e02B52de);
    Attribute[] public attributes; 
    
    /* TRANSFER PROCESS PROPERTIES */
    address public buyer;
    enum TRANSFER_STATE {NOT_FOR_SALE, FOR_SALE, PURCHASE_READY}
    TRANSFER_STATE private LandTitleTransferState = TRANSFER_STATE.NOT_FOR_SALE;

    function setLandAddress(string memory landAddressInput) public returns (uint) {
        landAddress = landAddressInput;
        return 0;
    }

    /* ------------------------
        CONSTRUCTION
     ------------------------ */
     
    constructor () {
        owner = msg.sender;
        buyer = address(0);     
    }
    
    /* ------------------------
        GENERAL IMPLEMENTATION
     ------------------------ */

    function getLandAddress() public view returns (string memory) {
        return landAddress;
    }

    function requestLandValuation() public {
        valueOracle.requestLandValue(landAddress);
    }

    /// @notice Generally Implemented after request to the oracle has taken place.  Returns the value of the land title
    /// @return unsigned integer denoting the value of the land address
    function getLandValue() public view returns (int){
        return int(baseValue) + addedValue;
    }

    /// @notice returns the added value of the land title
    /// @return integer denoting the added value
    function getAddedLandValue() public view returns (int) {return addedValue;}

    /// @notice returns the base value of the land title
    /// @return an insigne integer denoting the base value
    function getBaseLandvalue() public view returns (uint) {return baseValue;}

    /// @notice updates the value of the land title based on the given integer, only executable via land oracle
    /// @param newBaseValue the new base land value
    /// @return boolean indicating whether or not the function was successful
    function updateBaseLandValue(int newBaseValue) public authorisedOracle returns (bool) {
        baseValue = uint(newBaseValue); return true;
    }

    /// @notice updates the value of the land title based on the given integer, only executable internally
    /// @param newAddedValue the new value we set the kand title value to
    /// @return integer indicating the success code of the function
    function updateAddedLandValue(int newAddedValue) private returns (bool) {
        addedValue = newAddedValue; return true;
    }

    /// @notice adds a new attribute to the land title and then adds to the addedvalue of the land title
    /// @param attributeType the type enum of the newly added attribute
    /// @param attributeAddedValue the added value of the attribute - can be negative or positive
    /// @param description A string description of the attribute
    /// @return a boolean indicating whether the function executed correctly
    function addAttribute(Attribute.AttrType attributeType, int attributeAddedValue, string memory description) public checkOwnership returns (bool) {
        /* Create a new attribute and push it onto our list of attributes */
        Attribute a = new Attribute();
        a.setAttributeType(attributeType);
        a.setAddedValue(attributeAddedValue);
        a.setDescription(description);
        attributes.push(a);
        /* Update the land value */
        updateAddedLandValue(attributeAddedValue + addedValue);
        return true;
    }

    /// @notice removes an attribute at the given index
    /// @param indexToDelete the index which we are deleting from the list of attributes
    /// @return boolean indicating the outcome of the function
    function removeAttribute(uint indexToDelete) public checkOwnership returns (bool) {

        /* Check if the index is valid for deletion */
        if (indexToDelete >= attributes.length){return false;}

        /* Now that we now it's passable, remove the addedValue from the land title added value */
        updateAddedLandValue(addedValue - attributes[indexToDelete].getAddedValue());

        /* Otherwise, overwrite attribute fields from index onwards */
        for (uint index = indexToDelete; index < attributes.length - 1; index++){
            attributes[index] = attributes[index + 1];
        }

        /* now pop off final element that we no longer require */
        attributes.pop();
        return true;
    }

    /// @notice this method gets the attribute at the given index
    /// @param index the given index we are fetching from the list of attributes
    /// @return an Attribute contract object
    function getAttribute(uint index) public returns (Attribute) {
        if (index >= attributes.length){return new Attribute();}
        return attributes[index];
    }

    function updateTaxValue(uint tax) public  returns (uint) {
        taxValue = tax;
        return 0;
    }
    
    /* ------------------------
        STATE MACHINE for OWNERSHIP TRANSFER PROCESS
     ------------------------ */

    /// @notice Initiated when the owner wishes to mark this land title for sale
    ///  - Set the state to on sale for buyers to be able to view
    function requestSale() public checkOwnership checkNOTFORSALE returns (bool){
        LandTitleTransferState = TRANSFER_STATE.FOR_SALE;
        return true;
    }

    /// @notice Initiated when the owner wishes to terminate the sale of a land title
    function cancelSale() public checkOwnership checkFORSALE returns (bool){
        LandTitleTransferState = TRANSFER_STATE.NOT_FOR_SALE;
        return true;
    }
    
    /// @notice Initiated when a buyer wishes to purchase this land title
    /// - If the buyer field is already full, don't accept any other purchase offers
    /// - If the user requesting ownership is already the owner, don't accept the purchase offer
    /// - Set the sender as the new buyer
    /// - Set the state to be purchasable
    function requestOwnership () public checkDisownership checkFORSALE returns (bool){
        if (buyer != address(0)){return false;}
        buyer = tx.origin;
        LandTitleTransferState = TRANSFER_STATE.PURCHASE_READY;
        return true;
    }
    
    /// @notice Initiated when there is a buyer ready to purchase the land title and the owner wants to approve
    /// - Set the new owner to the buyer of the land title
    /// - Set the current buyer to a null address
    /// - Set the state of the land title to not for sale
    function approveOwnership() public checkOwnership checkPURCHASEREADY returns (bool){
        owner = buyer;
        buyer = address(0);
        LandTitleTransferState = TRANSFER_STATE.NOT_FOR_SALE;
        return true;
    }

    /// @notice Initiated when the owner has noit received funds or if suspected ownership theft
    function blockOwnership() public checkOwnership checkPURCHASEREADY returns (bool){
        buyer = address(0);
        LandTitleTransferState = TRANSFER_STATE.FOR_SALE;
        return true;
    }
    
    /* ------------------------
        MODIFIERS
     ------------------------ */
    
    /// @notice Can only execute when the land title is in the NOT_FOR_SALE state
    modifier checkNOTFORSALE() {
        require(LandTitleTransferState == TRANSFER_STATE.NOT_FOR_SALE, "Title State must be NOT FOR SALE");
        _;
    }
    
    /// @notice Can only execute when the land title is in the FOR_SALE state
    modifier checkFORSALE() {
        require(LandTitleTransferState == TRANSFER_STATE.FOR_SALE, "Title State must be FOR SALE");
        _;
    }
    
    /// @notice Can only execute when the land title is in the PURCHASE_READY state
    modifier checkPURCHASEREADY() {
        require(LandTitleTransferState == TRANSFER_STATE.PURCHASE_READY, "Title State must be PURCHASE READY");
        _;
    }

    /// @notice Can only execute when the caller of the method is the owner of the land title
    modifier checkOwnership() {
        require(tx.origin == owner, "You must be the owner to execute this method!");
        _;
    }

    /// @notice Can only execute when the caller of the method is no the owner of the land title
    modifier checkDisownership() {
        require(tx.origin != owner, "You must not be the owner to execute this method!");
        _;
    }

    modifier authorisedOracle() {
        require(msg.sender == address(valueOracle), "Function can only be called by authorised oracle");
        _;
    }
}
