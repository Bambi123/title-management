// SPDX-License-Identifier: UNLICENSED

pragma solidity ^0.8.0;
import "../contract-land-title/LandTitle.sol";

/// @title Oracle handling land value requests
contract TaxOracle {

    mapping(string => address) public pendingRequests;
    
    /* The event to be broadcast on the blockchain (listened to by off-chain components) */
    event TaxRequest(address requestOrigin, uint landValue, string landAddress);

    /// @notice receives the land value request and calls the off chain tax calculator
    /// @param landValue an unsigned integer that takes the land value
    /// @param landAddress a string for the postal address of the land property
    function requestTaxValue(uint landValue, string memory landAddress) public {
        pendingRequests[landAddress] = msg.sender;
        emit TaxRequest(msg.sender,landValue,landAddress);
    }

    /// @notice returns the tax value to the land value address
    /// @param landAddress a string for the postal address of the land property
    /// @param tax an unsigned integer denoting the amount of tax owed
    function returnTaxValue(string memory landAddress,uint tax) public {
        LandTitle returningLandTitle = LandTitle(pendingRequests[landAddress]);
        returningLandTitle.updateTaxValue(tax);
    }

    /// @notice Instantiates a new TaxOracle Smart Contract
    constructor () {}

}
