const axios = require('axios');
const querystring = require('querystring')
const Web3 = require('web3');

// CoreLogic API credentials
const client_id = 'myiPXUZ4EArhCJApbgyquCS62Qk36Bgu';
const client_secret = 'n3A5HBT3wGIxawLu';

// Web3 / Infura connection
let web3 = new Web3(Web3.givenProvider || 'wss://ropsten.infura.io/ws/v3/7f61bbc84e714c09b08432ff18934fbb');
let ABI = require('./valueOracleABI.json');
let valueOracleAddress = '0x8A7c6De03C283535b3F7A480D68e256c47514c14';

// //create instance of contract
let contract = new web3.eth.Contract(ABI, valueOracleAddress);

// Creating a signing account from a private key
const signer = web3.eth.accounts.privateKeyToAccount(
    process.env.SIGNER_PRIVATE_KEY_VALUE
);
web3.eth.accounts.wallet.add(signer);

console.log("STARTING OFF-CHAIN SCRIPT");

////////////////////////////////////////////////////////
//
//  Corelogic API call functions
//
const authorizeAPI_request = async () => {
    try {
        const res = await axios.get(`https://access-api.corelogic.asia/access/oauth/token?grant_type=client_credentials&client_id=${client_id}&client_secret=${client_secret}`, {
            headers: {
                'Content-Type': 'application/json'
            }
        });
        return res.data.access_token;
    } catch (err) {
        console.error(err);
    }
}

// API call to get the ID of the property in CoreLogic's database
const matchAddress = async (address, access_token) => {
    try {
        options = {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + access_token
            },
            params: {
                'q': address
            }
        }
        const res = await axios.get(`https://api-uat.corelogic.asia/sandbox/search/au/matcher/address?`, options);
        return res.data;
    } catch (err) {
        console.error(err);
    }
}

// API call to retrieve valuation estimate for the property
const getValuationEstimate = async (propertyID, access_token) => {
    console.log("> Querying value for propertyId:", propertyID);
    try {
        const res = await axios.get(`https://api-uat.corelogic.asia/sandbox/avm/au/properties/${propertyID}/avm/intellival/consumer/current`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + access_token
            }, 
            params: {

            }
        });
        return res.data;
    } catch (err) {
        //console.error(err);
    }
}

//
async function returnValueToOracle(address, value) {
    const tx = contract.methods.returnLandValue(address, value);
    const receipt = await tx
        .send({
            from: signer.address,
            gas: await tx.estimateGas() + 50000,
        })
        .once("transactionHash", (txhash) => {
            console.log(`> Mining transaction`);
            console.log(`https://ropsten.etherscan.io/tx/${txhash}`);
        });
}


////////////////////////////////////////////////////////
//
//  Main Program Script
//

// Get an access token from the api
authorizeAPI_request().then(access_token => {
    console.log("> Obtained API access token");
    return access_token;
}).then(access_token => {

    // Listen to the deployed ValueOracle contract for ValueRequest Events
    web3.eth.subscribe("logs", {address: valueOracleAddress}, (err, result) => {
        let decoded = web3.eth.abi.decodeLog([{
            type: 'address',
            name: 'requestOrigin'
        },{
            type: 'string',
            name: 'landAddress'
        }], result.data, result.topics);
        console.log("> New value request:");
        console.log(decoded, '\n');

        // Search for the property ID in the api based on address
        let address = decoded['1'];
        matchAddress(address, access_token).then(data => {
            console.log("> Match found in API database:");
            console.log(data.matchDetails, '\n');
            return data.matchDetails.propertyId;
        }).then(propertyID => {

            // Request a value estimate for the property ID
            getValuationEstimate(propertyID, access_token).then(data => {
                console.log("> Value Received, returning to oracle");

                // NOTE: some addresses have too little information in the api to get a value estimate
                // If this is the case, return 1000000 by default to demonstrate functionality.
                if (data == null) {
                    returnValueToOracle(address, 1000000);
                }
            })
        });
    });

});


