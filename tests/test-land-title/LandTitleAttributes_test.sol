// SPDX-License-Identifier: UNLICENSED

/* Solodity Version */
pragma solidity ^0.8.0;
/* Imported Testing Libraries */
import "remix_tests.sol";
import "remix_accounts.sol";
/* Imported Smart Contracts */
import "../../contracts/contract-land-title/LandTitle.sol";
import "../../contracts/contract-attribute/Attribute.sol";

/// @title Contract initiating testing for the Land Title contract
contract LandTitleAttributesTest is LandTitle {

    address acc0; address acc1;

    function beforeAll() public {
        acc0 = TestsAccounts.getAccount(0);
        acc1 = TestsAccounts.getAccount(1);
    }

    /// #sender: account-0
    function testIsOwner() public {
        Assert.ok(acc0 == owner, "Failed Unexpectedly");
    }

    /* ------------------------
        ATTRIBUTES MANAGED BY LANDTITLE
     ------------------------ */

    /// #sender: account-0
    function testInitialAttributes() public {
        Assert.equal(attributes.length, 0, "Failed Unexpectedly");
        Assert.equal(this.getBaseLandvalue(), 0, "Failed Unexpectedly");
        Assert.equal(this.getAddedLandValue(), 0, "Failed Unexpectedly");
        Assert.equal(this.getLandValue(), 0, "Failed Unexpectedly");
    }

    /// #sender: account-0
    function testAddAttribute() public {
        Assert.ok(this.addAttribute(Attribute.AttrType.RENNOVATION, 10, "Fix the hole in the roof"), "Failed Unexpectedly");
        Assert.equal(this.getBaseLandvalue(), 0, "Failed Unexpectedly");
        Assert.equal(this.getAddedLandValue(), 10, "Failed Unexpectedly");
        Assert.equal(this.getLandValue(), 10, "Failed Unexpectedly");

        Assert.ok(this.addAttribute(Attribute.AttrType.DAMAGE, -5, "Oops, the hole opened again"), "Failed Unexpectedly");
        Assert.equal(this.getBaseLandvalue(), 0, "Failed Unexpectedly");
        Assert.equal(this.getAddedLandValue(), 5, "Failed Unexpectedly");
        Assert.equal(this.getLandValue(), 5, "Failed Unexpectedly");

        Assert.ok(this.addAttribute(Attribute.AttrType.GENERAL, 20, "Yo my wife built a deck!"), "Failed Unexpectedly");
        Assert.equal(this.getBaseLandvalue(), 0, "Failed Unexpectedly");
        Assert.equal(this.getAddedLandValue(), 25, "Failed Unexpectedly");
        Assert.equal(this.getLandValue(), 25, "Failed Unexpectedly");
    }

    /// #sender: account-1
    function testAddAttributeNonOwner() public {
        try this.addAttribute(Attribute.AttrType.RENNOVATION, 10, "Fix the hole in the roof") returns (bool) {
            Assert.ok(false, "Method execution should fail");
        } catch Error(string memory reason){
            Assert.equal(reason, "You must be the owner to execute this method!", "Failed with unexpected reason");
        } catch (bytes memory){
            Assert.ok(false, "Failed unexpected");
        } 
    }

    /// #sender: account-0
    function testGetAttribute() public {
        Attribute a = this.getAttribute(0);
        Assert.ok(a.getAttributeType() == Attribute.AttrType.RENNOVATION, "Failed Unexpectedly");
        Assert.ok(a.getAddedValue() == 10, "Failed Unexpectedly");
        Assert.ok(keccak256(abi.encodePacked(a.getDescription())) == keccak256(abi.encodePacked("Fix the hole in the roof")), "Failed Unexpectedly");
    }

    /// #sender: account-1
    function testGetAttributeNonOwner() public {
        Attribute a = this.getAttribute(0);
        Assert.ok(a.getAttributeType() == Attribute.AttrType.RENNOVATION, "Failed Unexpectedly");
        Assert.ok(a.getAddedValue() == 10, "Failed Unexpectedly");
        Assert.ok(keccak256(abi.encodePacked(a.getDescription())) == keccak256(abi.encodePacked("Fix the hole in the roof")), "Failed Unexpectedly");
    }

    /// #sender: account-1
    function testRemoveAttributeNonOwner() public {
        /* Attempt to Remove the new Attribute */
        try this.removeAttribute(1) returns (bool){
            Assert.ok(false, "Method execution should fail");
        } catch Error(string memory reason){
            Assert.equal(reason, "You must be the owner to execute this method!", "Failed with unexpected reason");
        } catch (bytes memory){
            Assert.ok(false, "Failed unexpected");
        } 
    }

    /// #sender: account-0
    function testRemoveAttribute() public {
        Assert.ok(this.removeAttribute(1), "Failed Unexpectedly");

        Attribute a = this.getAttribute(0);
        Assert.ok(a.getAttributeType() == Attribute.AttrType.RENNOVATION, "Failed Unexpectedly");
        Assert.ok(a.getAddedValue() == 10, "Failed Unexpectedly");
        Assert.ok(keccak256(abi.encodePacked(a.getDescription())) == keccak256(abi.encodePacked("Fix the hole in the roof")), "Failed Unexpectedly");  
    
    
        Attribute b = this.getAttribute(1);
        Assert.ok(b.getAttributeType() == Attribute.AttrType.GENERAL, "Failed Unexpectedly");
        Assert.ok(b.getAddedValue() == 20, "Failed Unexpectedly");
        Assert.ok(keccak256(abi.encodePacked(b.getDescription())) == keccak256(abi.encodePacked("Yo my wife built a deck!")), "Failed Unexpectedly"); 
    
        Assert.equal(this.getBaseLandvalue(), 0, "Failed Unexpectedly");
        Assert.equal(this.getAddedLandValue(), 30, "Failed Unexpectedly");
        Assert.equal(this.getLandValue(), 30, "Failed Unexpectedly");
    }
}