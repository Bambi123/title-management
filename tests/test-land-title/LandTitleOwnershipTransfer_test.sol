// SPDX-License-Identifier: UNLICENSED

/* Solodity Version */
pragma solidity >= 0.8.00 < 0.9.0;
/* Imported Testing Libraries */
import "remix_tests.sol";
import "remix_accounts.sol";
/* Imported Smart Contract */
import "../../contracts/contract-land-title/LandTitle.sol";

contract LandTitleOwnershipTransferTest is LandTitle {

    /* Account variables set up to emulate the addresses of the different accounts */
    address acc0; address acc1; address acc2;
    
    function beforeAll() public {
        acc0 = TestsAccounts.getAccount(0);
        acc1 = TestsAccounts.getAccount(1);
        acc2 = TestsAccounts.getAccount(2);
    }

    function testOwnershp() public {
        Assert.equal(owner, acc0, "Owner should be acc0");
    }

    function testBuyer() public {
        Assert.equal(buyer, address(0), "Buyer should be initially set to null");
    }
    
    /* ------------------------
        REQUESTING SALE
     ------------------------ */

    /// #sender: account-1
    function testRequestSaleAsNonOwner() public {
        try this.requestSale() returns (bool) {
            Assert.ok(false, "Method execution should fail");
        } catch Error(string memory reason){
            Assert.equal(reason, "You must be the owner to execute this method!", "Failed with unexpected reason");
        } catch (bytes memory){
            Assert.ok(false, "Failed unexpected");
        }
    }
    
    /// #sender: account-0
    function testRequestSaleAsOwner() public {
        Assert.ok(this.requestSale(), "Failed unexpected");
    }
    
    /// #sender: account-0
    function testRequestSaleAgainAsOwner() public {
        try this.requestSale() returns (bool) {
            Assert.ok(false, "Method execution should fail");
        } catch Error(string memory reason){
            Assert.equal(reason, "Title State must be NOT FOR SALE", "Failed with unexpected reason");
        } catch (bytes memory){
            Assert.ok(false, "Failed unexpected");
        }
    }
    
    /* ------------------------
        REQUESTING OWNERSHIP
     ------------------------ */
    
    /// #sender: account-0
    function testRequestOwnershipAsOwner() public {
        try this.requestOwnership() returns (bool) {
            Assert.ok(false, "Method execution should fail");
        } catch Error(string memory reason){
            Assert.equal(reason, "You must not be the owner to execute this method!", "Failed with unexpected reason");
        } catch (bytes memory){
            Assert.ok(false, "Failed unexpected");
        }
    }
    
    /// #sender: account-1
    function testRequestOwnershipAsBuyer() public {
        Assert.ok(this.requestOwnership(), "Failed unexpected");
    }
    
    /// #sender: account-2
    function testRequestOwnershipAsAnotherBuyer() public {
        try this.requestOwnership() returns (bool) {
            Assert.ok(false, "Method execution should fail");
        } catch Error(string memory reason){
            Assert.equal(reason, "Title State must be FOR SALE", "Failed with unexpected reason");
        } catch (bytes memory){
            Assert.ok(false, "Failed unexpected");
        }
    }

    /// #sender: account-1
    function testNewBuyer() public {
        Assert.equal(buyer, acc1, "New Buyer Should be now Initialised");
    }
    
   /* ------------------------
        APPROVE OWNERSHIP
     ------------------------ */
    
    /// #sender: account-1
    function testApproveOwnershipAsBuyer() public {
        try this.approveOwnership() returns (bool) {
            Assert.ok(false, "Method execution should fail");
        } catch Error(string memory reason){
            Assert.equal(reason, "You must be the owner to execute this method!", "Failed with unexpected reason");
        } catch (bytes memory){
            Assert.ok(false, "Failed unexpected");
        }
    }
    
    /// #sender: account-0
    function testApproveOwnershipAsOwner() public {
        Assert.ok(this.approveOwnership(), "Failed unexpected");
    }
    
    /// #sender: account-0
    function testApproveOwnershipAsPreviousOwner() public {
        try this.approveOwnership() returns (bool) {
            Assert.ok(false, "Method execution should fail");
        } catch Error(string memory reason){
            Assert.equal(reason, "You must be the owner to execute this method!", "Failed with unexpected reason");
        } catch (bytes memory){
            Assert.ok(false, "Failed unexpected");
        }  
    }

    /// #sender: account-0
    function testNewOwner() public {
        Assert.equal(owner, acc1, "New Owner Should be now Initialised");
    }

    /// #sender: account-0
    function testNextBuyer() public {
        Assert.equal(buyer, address(0), "Current Buyer should be reinitialised back to null");
    }
    
    /// #sender: account-1
    function testApproveOwnershipAsNewOwner() public {
        try this.approveOwnership() returns (bool) {
            Assert.ok(false, "Method execution should fail");
        } catch Error(string memory reason){
            Assert.equal(reason, "Title State must be PURCHASE READY", "Failed with unexpected reason");
        } catch (bytes memory){
            Assert.ok(false, "Failed unexpected");
        }  
    }

    /* ------------------------
        BLOCKING OWNERSHIP
     ------------------------ */

     /// #sender: account-1
    function testPrepareSale() public {
        Assert.ok(this.requestSale(), "Failed Unexpectedly");
    }

    /// #sender: account-0
    function testPrepareOwnership() public {
        Assert.ok(this.requestOwnership(), "Failed Unexpectedly");
    }

    /// #sender: account-0
    function testBlockOwnershipNonOwner() public {
        try this.approveOwnership() returns (bool) {
            Assert.ok(false, "Method execution should fail");
        } catch Error(string memory reason){
            Assert.equal(reason, "You must be the owner to execute this method!", "Failed with unexpected reason");
        } catch (bytes memory){
            Assert.ok(false, "Failed unexpected");
        } 
    }

    /// #sender: account-1
    function testBlockOwnership() public {
        Assert.ok(this.blockOwnership(), "Failed Unexpectedly");
    }

    /// #sender: account-1
    function testBlockOwnershipAfterBlock() public {
        try this.approveOwnership() returns (bool) {
            Assert.ok(false, "Method execution should fail");
        } catch Error(string memory reason){
            Assert.equal(reason, "Title State must be PURCHASE READY", "Failed with unexpected reason");
        } catch (bytes memory){
            Assert.ok(false, "Failed unexpected");
        } 
    }

    /* ------------------------
        CANCELLING SALE
     ------------------------ */

    /// #sender: account-0
    function testCancelSaleNonowner() public {
        try this.cancelSale() returns (bool) {
            Assert.ok(false, "Method execution should fail");
        } catch Error(string memory reason){
            Assert.equal(reason, "You must be the owner to execute this method!", "Failed with unexpected reason");
        } catch (bytes memory){
            Assert.ok(false, "Failed unexpected");
        } 
    }

    /// #sender: account-1
    function testCancelSale() public {
        Assert.ok(this.cancelSale(), "Failed Unexpectedly");
    }

    /// #sender: account-1
    function testCancelSaleAfterCancel() public {
        try this.cancelSale() returns (bool) {
            Assert.ok(false, "Method execution should fail");
        } catch Error(string memory reason){
            Assert.equal(reason, "Title State must be FOR SALE", "Failed with unexpected reason");
        } catch (bytes memory){
            Assert.ok(false, "Failed unexpected");
        } 
    }
}