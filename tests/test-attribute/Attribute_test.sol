// SPDX-License-Identifier: UNLICENSED

/* Solodity Version */
pragma solidity ^0.8.0;
/* Imported Testing Libraries */
import "remix_tests.sol";
import "remix_accounts.sol";
/* Imported Smart Contract */
import "../../contracts/contract-attribute/Attribute.sol";

contract AttributeTest is Attribute {

    /// #sender: account-0
    function testAttributeTypeGENERAL() public {
        Assert.ok(this.getAttributeType() == AttrType.GENERAL, "Initialised Attribute Type should be GENERAL");
    }

    /// #sender: account-0
    function testAttributeTypeDAMAGE() public {
        Assert.ok(this.setAttributeType(AttrType.DAMAGE), "Failed unexpectedly");
        Assert.ok(this.getAttributeType() == AttrType.DAMAGE, "Attribute Type should be DAMAGED");
    }

    /// #sender: account-0
    function testAttributeTypeRENNOVATION() public {
        Assert.ok(this.setAttributeType(AttrType.RENNOVATION), "Failed unexpectedly");
        Assert.ok(this.getAttributeType() == AttrType.RENNOVATION, "Attribute Type should be RENNOVATION");
    }

    /// #sender: account-0
    function testInitialisedAddedValue() public {
        Assert.ok(this.getAddedValue() == 0, "Initialied Added Value should be 0");
    }

    /// #sender: account-0
    function testSetAddedValue() public {
        Assert.ok(this.setAddedValue(123), "Failed Unexpectedly");
        Assert.ok(this.getAddedValue() == 123, "Added Value should be 123");
    }

    /// #sender: account-0
    function testInitialisedDescription() public {
        Assert.ok(keccak256(abi.encodePacked(this.getDescription())) == keccak256(abi.encodePacked("")), "Initialied Added Value should be 0");
    }

    /// #sender: account-0
    function testSetDescription() public {
        Assert.ok(this.setDescription("nice"), "Failed unexpectedly");
        Assert.ok(keccak256(abi.encodePacked(this.getDescription())) == keccak256(abi.encodePacked("nice")), "Description should be 'nice'");
    }

}