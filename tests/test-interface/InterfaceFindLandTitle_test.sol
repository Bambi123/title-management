// SPDX-License-Identifier: UNLICENSED

/* Solodity Version */
pragma solidity ^0.8.0;
/* Imported Testing Libraries */
import "remix_tests.sol";
import "remix_accounts.sol";
/* Imported Smart Contract */
import "../../contracts/contract-interface/Interface.sol";

contract InterfaceFindLandTitleTest is Interface {

    /* Account variables set up to emulate the addresses of the different accounts */
    address acc0; address acc1;

    /* Variable to store specific testing address of called upon smart contracts */
    address TEST; address TEST2;
    
    function beforeAll() public {
        acc0 = TestsAccounts.getAccount(0);
        acc1 = TestsAccounts.getAccount(1);
        TEST = address(0);
        TEST2 = address(0);
    }

    /// #sender: account-0
    function testAdministrator() public {
        Assert.equal(this.getAdministrator(), acc0, "The administrator should be account 0!");
        Assert.equal(this.getLandTitleDatabaselength(), 0, "Initialised Database should be empty!");
    }

    /* ------------------------
        FIND LAND TITLE
     ------------------------ */

    /// #sender: account-0
    function testFindLandTitleSetup() public {
        TEST = this.registerLandTitle("1 Todman Avenue, Kensington, NSW"); 
        TEST2 = this.registerLandTitle("2 Todman Avenue, Kensington, NSW");
        Assert.equal(this.getLandTitleDatabaselength(), 2, "Database should be length 2!");
    }

    /// #sender: account-1
    function testFindLandTitle() public {
        Assert.equal(TEST, this.findLandTitle("1 Todman Avenue, Kensington, NSW"), "Failed Unexpectedly");
        Assert.equal(TEST2, this.findLandTitle("2 Todman Avenue, Kensington, NSW"), "Failed Unexpectedly");
    } 

    /// #sender: account-0 
    function testFindLandTitleNonExistentTitle() public {
        Assert.equal(this.findLandTitle("3 Todman Avenue, Kensington, NSW"), address(0), "Failed Unexpectedly");
    }

    /// #sender: account-0
    function testFindLandTitleValidIndex() public {
        Assert.equal(this.findLandTitle("1 Todman Avenue, Kensington, NSW"), this.findLandTitle(0), "Failed Unexpectedly");
        Assert.equal(this.findLandTitle("2 Todman Avenue, Kensington, NSW"), this.findLandTitle(1), "Failed Unexpectedly");
    }

    /// #sender: account-0
    function testFindLandTitleFaultyIndex() public {
        Assert.equal(this.findLandTitle(3), address(0), "Failed Unexpectedly");
    }

}