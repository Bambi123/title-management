// SPDX-License-Identifier: UNLICENSED

/* Solodity Version */
pragma solidity ^0.8.0;
/* Imported Testing Libraries */
import "remix_tests.sol";
import "remix_accounts.sol";
/* Imported Smart Contract */
import "../../contracts/contract-interface/Interface.sol";

contract InterfaceRemoveLandTitleTest is Interface {

    /* Account variables set up to emulate the addresses of the different accounts */
    address acc0; address acc1;
    
    function beforeAll() public {
        acc0 = TestsAccounts.getAccount(0);
        acc1 = TestsAccounts.getAccount(1);
    }

    /// #sender: account-0
    function testAdministrator() public {
        Assert.equal(this.getAdministrator(), acc0, "The administrator should be account 0!");
        Assert.equal(this.getLandTitleDatabaselength(), 0, "Initialised Database should be empty!");
    }

    /* ------------------------
        REMOVE LAND TITLE
     ------------------------ */

    /// #sender: account-0
    function testRemoveLandTitleSetup() public {
        this.registerLandTitle("1 Todman Avenue, Kensington, NSW"); 
        this.registerLandTitle("2 Todman Avenue, Kensington, NSW"); 
        this.registerLandTitle("3 Todman Avenue, Kensington, NSW"); 
        Assert.equal(this.getLandTitleDatabaselength(), 3, "Database should be length 1!!");
    }

    /// #sender: account-1
    function testRemoveLandTitleNonAdmin() public {
        try this.removeLandTitle("2 Todman Avenue, Kensington, NSW") returns (bool) {
            Assert.ok(false, "Failed unexpected");
        } catch Error(string memory reason){
            Assert.equal(reason, "You must be an administrator to execute this method!", "Failed with unexpected reason");
        } catch (bytes memory){
            Assert.ok(false, "Failed unexpected");
        }
    }

    /// #sender: account-0
    function testRemoveLandTitle() public {
        Assert.ok(this.removeLandTitle("2 Todman Avenue, Kensington, NSW"), "Failed Expectedly");

        Assert.equal(this.getLandTitleDatabaselength(), 2, "Failed Unexpectedly");
        Assert.equal(this.findLandTitle("1 Todman Avenue, Kensington, NSW"), this.findLandTitle(0), "Failed Unexpectedly");
        Assert.equal(this.findLandTitle("3 Todman Avenue, Kensington, NSW"), this.findLandTitle(1), "Failed Unexpectedly");
        Assert.equal(this.findLandTitle("2 Todman Avenue, Kensington, NSW"), address(0), "Failed Unexpectedly");
    }

}