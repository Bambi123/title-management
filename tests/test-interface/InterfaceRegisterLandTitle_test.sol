// SPDX-License-Identifier: UNLICENSED

/* Solodity Version */
pragma solidity ^0.8.0;
/* Imported Testing Libraries */
import "remix_tests.sol";
import "remix_accounts.sol";
/* Imported Smart Contract */
import "../../contracts/contract-interface/Interface.sol";

contract InterfaceRegisterLandTitleTest is Interface {

    /* Account variables set up to emulate the addresses of the different accounts */
    address acc0; address acc1;
    
    function beforeAll() public {
        acc0 = TestsAccounts.getAccount(0);
        acc1 = TestsAccounts.getAccount(1);
    }

    /// #sender: account-0
    function testAdministrator() public {
        Assert.equal(this.getAdministrator(), acc0, "The administrator should be account 0!");
        Assert.equal(this.getLandTitleDatabaselength(), 0, "Initialised Database should be empty!");
    }

    /* ------------------------
        REGISTER LAND TITLE
     ------------------------ */

    /// #sender: account-1
    function testRegisterLandTitleAsNonAdmin() public {
        try this.registerLandTitle("1 Todman Avenue, Kensington, NSW") returns (address) {
            Assert.ok(false, "Failed unexpected");
        } catch Error(string memory reason){
            Assert.equal(reason, "You must be an administrator to execute this method!", "Failed with unexpected reason");
        } catch (bytes memory){
            Assert.ok(false, "Failed unexpected");
        }
    }

    /// #sender: account-0
    function testRegisterLandTitle() public {
        try this.registerLandTitle("1 Todman Avenue, Kensington, NSW") returns (address) {
            Assert.ok(true, "Excellent!");
        } catch Error(string memory){
            Assert.ok(false, "Failed unexpected");
        } catch (bytes memory){
            Assert.ok(false, "Failed unexpected");
        }
    }

    /// #sender: account-0
    function testLandTitleDatabaseLengthOne() public {
        Assert.equal(this.getLandTitleDatabaselength(), 1, "Database should be length 1!!");
    }

    /// #sender: account-0
    function testRegisterLandTitleDuplicate() public {
        try this.registerLandTitle("1 Todman Avenue, Kensington, NSW") returns (address) {
            Assert.ok(false, "Failed unexpected");
        } catch Error(string memory reason){
            Assert.equal(reason, "This land address already exists!", "Failed with unexpected reason");
        } catch (bytes memory){
            Assert.ok(false, "Failed unexpected");
        }
    }
}