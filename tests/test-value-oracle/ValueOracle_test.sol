// SPDX-License-Identifier: UNLICENSED

/* Solodity Version */
pragma solidity >= 0.8.00 < 0.9.0;
/* Imported Testing Libraries */
import "remix_tests.sol";
import "remix_accounts.sol";
/* Imported Smart Contract */
import "../../contracts/contract-land-title/LandTitle.sol";
import "../../contracts/contract-value-oracle/ValueOracle.sol";

contract LandTitleTest is LandTitle {

    /* Account variables set up to emulate the addresses of the different accounts */
    address acc0;
    
    function beforeAll() public {
        acc0 = TestsAccounts.getAccount(0);
    }

    /* ------------------------
        REQUEST EVALUATION
     ------------------------ */

    /// #sender: account-0
    function testSetLandValue() public {
        string memory physicalAddress = "34 Ettalong St Wheeler Heights 2097";
        this.setLandAddress(physicalAddress);
        string memory result = this.getLandAddress();
        Assert.equal(physicalAddress, result, "It should store the correct address");
    }

    /// #sender: account-0
    function testInitialValue() public {
        int uninitialisedValuation = this.getLandValue();
        Assert.equal(uninitialisedValuation, 0, "Should be uninitialised at 0");
    }

    /* ------------------------
        RECEIVE EVALUATION
     ------------------------ */

    /// #sender: account-0
    function testOracleReceiveRequest() public {
        ValueOracle testOracle = new ValueOracle();
        string memory physicalAddress = "34 Ettalong St Wheeler Heights 2097";
        testOracle.requestLandValue(physicalAddress);
        Assert.equal(testOracle.pendingRequests(physicalAddress), address(this), "Should be 1 new pending request (from this this)");
    }

    /// #sender: account-0
    function testUnauthorisedOracleReturn() public {
        ValueOracle testOracle = new ValueOracle();
        string memory physicalAddress = "34 Ettalong St Wheeler Heights 2097";
        testOracle.requestLandValue(physicalAddress);

        try testOracle.returnLandValue(physicalAddress, 1000000) {
            Assert.ok(false, "Method execution should fail");
        } catch Error(string memory reason){
            Assert.equal(reason, "Function can only be called by authorised oracle", "Failed with unexpected reason");
        } catch (bytes memory){
            Assert.ok(false, "Failed unexpected");
        }
    }
}