// SPDX-License-Identifier: UNLICENSED

/* Solodity Version */
pragma solidity >= 0.8.00 < 0.9.0;
/* Imported Testing Libraries */
import "remix_tests.sol";
import "remix_accounts.sol";
/* Imported Smart Contract */
import "../../contracts/contract-land-title/LandTitle.sol";
import "../../contracts/contract-tax-oracle/TaxOracle.sol";

contract LandTitleTest is LandTitle {

    /* Account variables set up to emulate the addresses of the different accounts */
    address acc0;
    
    function beforeAll() public {
        acc0 = TestsAccounts.getAccount(0);
    }

    /* ------------------------
        REQUEST TAXATION
     ------------------------ */

    /// #sender: account-0
    function testSetLandTax() public {
        string memory physicalAddress = "34 Ettalong St Wheeler Heights 2097";
        this.setLandAddress(physicalAddress);
        string memory result = this.getLandAddress();
        Assert.equal(physicalAddress, result, "It should store the correct address");
    }

    /// #sender: account-0
    function testInitialTax() public {
        uint uninitialisedTax = this.taxValue();
        Assert.equal(uninitialisedTax, 0, "Should be uninitialised at 0");
    }

    /* ------------------------
        RECEIVE TAXATION
     ------------------------ */

    /// #sender: account-0
    function testOracleReceiveRequest() public {
        TaxOracle testOracle = new TaxOracle();
        string memory physicalAddress = "34 Ettalong St Wheeler Heights 2097";
        testOracle.requestTaxValue(1000,physicalAddress);
        Assert.equal(testOracle.pendingRequests(physicalAddress), address(this), "Should be 1 new pending request (from this this)");
    }


}
